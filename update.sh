#!/bin/bash

# ------------------------------------------------------------ HELLO
cat <<EOF
STUDIO       _       _
 _ __   ___ (_)_ __ | |_ ___ _ ____/\__
| '_ \ / _ \| | '_ \| __/ _ \ '__\\    /
| |_) | (_) | | | | | ||  __/ |  /_  _\\
| .__/ \___/|_|_| |_|\__\___|_|    \/
|_|
      presents: $0

EOF

# ------------------------------------------------------------ HELPER FUNCTIONS
decision() {
    # get default
    if [ "$2" = 0 ]; then
        default="yes"
    else
        default="no"
    fi

    # should we go autopilot
    if [ "${REMEMBORY_AUTOPILOT}" = 1 ];
    then
        echo "$1?"
        return $2
    fi

    echo ""
    echo "----------------------------------"
    echo "| $0 NEEDS A DECISION"
    echo "| "

    echo "| $1?"
    read -r -p "| Do you want? (y)es / (n)o / (d)efault - $default " input

    case $input in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        [nN][oO]|[nN])
            return 1
            ;;
        [dD])
            return $2
            ;;
        *)
            return 1
            ;;
    esac
}

# ------------------------------------------------------------ VARIABLES
# dynamic
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PREVIOUS_DIR=`pwd`

# defaults
REMEMBORY_AUTOPILOT=0
PROBABLY_YES=0
PROBABLY_NOT=1

cd $SCRIPT_DIR

OLD_ELEMENTWEB_VERSION=`grep "ARG ELEMENTWEB_VERSION" ./Dockerfile | grep -oP '"\K[^"\047]+(?=["\047])'`
NEW_ELEMENTWEB_VERSION=`./getLatestRelease.sh`

if [ $OLD_ELEMENTWEB_VERSION != $NEW_ELEMENTWEB_VERSION ];
then
    echo "'$OLD_ELEMENTWEB_VERSION' -> '$NEW_ELEMENTWEB_VERSION'"
    TMP_NAME=Dockerfile-`date --iso-8601`
    cp ./Dockerfile ./$TMP_NAME   
    echo "created temporary backup file $TMP_NAME"
    #sed -i ./$TMP_NAME '/ARG NEW_ELEMENTWEB_VERSION=/ s/="[^"][^"]*"/="$NEW_ELEMENTWEB_VERSION"/' ./Dockerfile
    sed -i "s/ARG ELEMENTWEB_VERSION=.*/ARG ELEMENTWEB_VERSION=\"$NEW_ELEMENTWEB_VERSION\"/g" ./Dockerfile
    if decision "remove the temporary backup file?" $PROBABLY_YES;
    then
        rm $TMP_NAME
    fi
    echo "You should now update the server, and possibly this repository"
    
fi

cd $PREVIOUS_DIR
